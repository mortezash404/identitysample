﻿using System.ComponentModel.DataAnnotations;

namespace IdentitySample.ViewModels.Account
{
    public class LoginViewModel
    {
        [Required,Display(Name = "نام کاربری")]
        public string Username { get; set; }

        [Required, Display(Name = "کلمه عبور")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "مرا به خاطر بسپار")]
        public bool RememberMe { get; set; }
    }
}
