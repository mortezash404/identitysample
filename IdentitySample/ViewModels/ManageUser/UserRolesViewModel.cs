﻿namespace IdentitySample.ViewModels.ManageUser
{
    public class UserRolesViewModel
    {
        public UserRolesViewModel()
        {
        }

        public UserRolesViewModel(string roleName)
        {
            RoleName = roleName;
        }

        public string RoleName { get; set; }

        public bool IsSelected { get; set; }
    }
}