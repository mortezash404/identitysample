﻿namespace IdentitySample.ViewModels.ManageUser
{
    public class ClaimsViewModel
    {
        
        public ClaimsViewModel()
        {
        }

        public ClaimsViewModel(string claimType)
        {
            ClaimType = claimType;
        }

        public string ClaimType { get; set; }
        public bool IsSelected { get; set; }
    }
}