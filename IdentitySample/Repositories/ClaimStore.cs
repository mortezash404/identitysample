﻿using System.Collections.Generic;
using System.Security.Claims;

namespace IdentitySample.Repositories
{
    public static class ClaimStore
    {
        public static List<Claim> AllClaims = new List<Claim>
        {
            new Claim(ClaimTypesStore.EmployeeList,true.ToString()),
            new Claim(ClaimTypesStore.EmployeeDetails,true.ToString()),
            new Claim(ClaimTypesStore.EmployeeEdit,true.ToString()),
            new Claim(ClaimTypesStore.AddEmployee,true.ToString())
        };
    }
}
